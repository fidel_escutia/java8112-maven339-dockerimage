- - -
#Oracle-Java-with-Maven Docker Imagefile

##
Docker base image: **`ubuntu:latest`**  
Oracle Java version: **`jdk-8.112.15`**  
Maven version: **`mvn-3.3.9`**  

##
These are the bash commands the Dockerfile's image will run at container creation:

	:::python
	apt-get update && apt-get install -y ca-certificates curl openssh-client && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
	cd ~
	mkdir -p /opt/java/current
	curl --cacert /etc/ssl/certs/GeoTrust_Global_CA.pem --header "Cookie: oraclelicense=accept-securebackup-cookie;" -fsSLO http://download.oracle.com/otn-pub/java/jdk/8u31-b13/jdk-8u31-linux-x64.tar.gz
	tar -xzf jdk-8u31-linux-x64.tar.gz -C /opt/java/current --strip-components=1
	rm -f jdk-8u31-linux-x64.tar.gz
	update-alternatives --install "/usr/bin/java" "java" "/opt/java/current/bin/java" 1
	update-alternatives --install "/usr/bin/javac" "javac" "/opt/java/current/bin/javac" 1
	update-alternatives --install "/usr/bin/javaws" "javaws" "/opt/java/current/bin/javaws" 1
	update-alternatives --set java /opt/java/current/bin/java
	update-alternatives --set javac /opt/java/current/bin/javac
	update-alternatives --set javaws /opt/java/current/bin/javaws

	mkdir -p /opt/maven/current
	curl -fsSLO http://www-us.apache.org/dist/maven/maven-3/3.2.5/binaries/apache-maven-3.2.5-bin.tar.gz
	tar -xzf apache-maven-3.2.5-bin.tar.gz -C /opt/maven/current --strip-components=1
	rm -f apache-maven-3.2.5-bin.tar.gz
	update-alternatives --install "/usr/bin/mvn" "mvn" "/opt/maven/current/bin/mvn" 1
	update-alternatives --set mvn /opt/maven/current/bin/mvn

	echo "" >> /etc/profile
	echo "JAVA_HOME=/opt/java/current" >> /etc/profile
	echo "MAVEN_HOME=/opt/maven/current" >> /etc/profile
	echo "PATH=$JAVA_HOME/bin:$MAVEN_HOME/bin:$PATH" >> /etc/profile
	echo "export JAVA_HOME" >> /etc/profile
	echo "export MAVEN_HOME" >> /etc/profile
	echo "export PATH" >> /etc/profile
	. /etc/profile

##
If you have a nexus-like maven repository server you could do something like the following where you first do ssh tunneling and
then run your maven command with a custom settings.xml like so:

####SSH KEY AUTHENTICATION:####
mkdir -p ~/.ssh  
cat bitbucket-crater_known_hosts >> ~/.ssh/known_hosts  
(umask  077 ; echo $MY_SSH_KEY | base64 --decode > ~/.ssh/id_rsa)  
ssh -Nf -L 8081:crater:8081 -p 40022 vescutia@mycompany.com  

####MAVEN COMMANDS####
export MAVEN_OPTS='-Xms1024m -Xmx2048m -XX:ReservedCodeCacheSize=128M'  
mvn -V -B -s settings.xml clean install  

#