FROM ubuntu:latest
MAINTAINER Victor F. Escutia <fidel.escutia@gmail.com>

ENV JAVA_VERSION 8
ENV JAVA_UPDATE 112
ENV JAVA_BUILD 15
ENV JAVA_HOME /opt/java/current
ENV MAVEN_VERSION 3.3.9
ENV MAVEN_HOME /opt/maven/current

RUN apt-get update && apt-get install -y \
    ca-certificates \
    curl \
    openssh-client \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir -p "${JAVA_HOME}" \
    && curl --cacert /etc/ssl/certs/GeoTrust_Global_CA.pem --header "Cookie: oraclelicense=accept-securebackup-cookie;" \
    -fsSL http://download.oracle.com/otn-pub/java/jdk/"${JAVA_VERSION}"u"${JAVA_UPDATE}"-b"${JAVA_BUILD}"/jdk-"${JAVA_VERSION}"u"${JAVA_UPDATE}"-linux-x64.tar.gz \
    | tar -xzC "${JAVA_HOME}" --strip-components=1

RUN update-alternatives --install "/usr/bin/java" "java" "${JAVA_HOME}/bin/java" 1 \
    && update-alternatives --install "/usr/bin/javac" "javac" "${JAVA_HOME}/bin/javac" 1 \
    && update-alternatives --install "/usr/bin/javaws" "javaws" "${JAVA_HOME}/bin/javaws" 1 \
    && update-alternatives --set java "${JAVA_HOME}"/bin/java \
    && update-alternatives --set javac "${JAVA_HOME}"/bin/javac \
    && update-alternatives --set javaws "${JAVA_HOME}"/bin/javaws

RUN mkdir -p "${MAVEN_HOME}" \
    && curl -fsSL http://www-us.apache.org/dist/maven/maven-3/"${MAVEN_VERSION}"/binaries/apache-maven-"${MAVEN_VERSION}"-bin.tar.gz \
    | tar -xzC "${MAVEN_HOME}" --strip-components=1

RUN update-alternatives --install "/usr/bin/mvn" "mvn" "${MAVEN_HOME}/bin/mvn" 1 \
    && update-alternatives --set mvn "${MAVEN_HOME}"/bin/mvn

RUN echo "" >> /etc/profile \
    && echo "JAVA_HOME=${JAVA_HOME}" >> /etc/profile \
    && echo "MAVEN_HOME=${MAVEN_HOME}" >> /etc/profile \
    && echo "PATH=$JAVA_HOME/bin:$MAVEN_HOME/bin:$PATH" >> /etc/profile \
    && echo "export JAVA_HOME" >> /etc/profile \
    && echo "export MAVEN_HOME" >> /etc/profile \
    && echo "export PATH" >> /etc/profile


